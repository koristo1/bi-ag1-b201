#ifndef __PROGTEST__

#include "flib.h"

#endif //__PROGTEST__

static const int FILE_OVERHEAD = 12;
static const int FIRST_TEMPORARY_FILE = 2;
static const size_t INT32_T_BYTES = sizeof(int32_t);
static const int64_t INT64_T_MY_MAX = 9223372036854775807;

void swap(int32_t *i, int32_t *i1) {
    int dummy = *i;
    *i = *i1;
    *i1 = dummy;
}

int ceil(double num) {
    int inum = (int) num;

    return inum + (num != (double) inum);
}

struct InputStream {
public:
    const int32_t ID;
    int32_t current;
    int32_t size;
    int32_t *data;

    InputStream(int32_t ID, int32_t size) : ID(ID), current(0), size(size), data(new int32_t[size]) {
        flib_open(ID, READ);
        read();
    }

    ~InputStream() {
        flib_close(ID);
        flib_remove(ID);
        delete[] data;
    }

    bool empty() const {
        return current == size;
    }

    int64_t first() const {
        return size != 0 ? (int64_t) data[current] : INT64_T_MY_MAX;
    }

    void shift() {
        ++current;

        if (empty())
            read();
    }

    void read() {
        current = 0;
        size = flib_read(ID, data, size);
    }
};

struct OutputStream {
public:
    int32_t ID;
    int32_t current;
    int32_t size;
    int32_t *data;

    OutputStream(int32_t ID, int32_t size) : ID(ID), current(0), size(size), data(new int32_t[size]) {
        flib_open(ID, WRITE);
    }

    ~OutputStream() {
        flib_close(ID);
        delete[] data;
    }

    bool full() const {
        return current == size;
    }

    void flush() {
        flib_write(ID, data, current);
    }

    void write(int32_t value) {
        if (full()) {
            flib_write(ID, data, current);
            current = 0;
        }

        data[current] = value;
        current++;
    }

    void copy(int32_t inputID) const {
        flib_open(inputID, READ);

        int32_t readSize;

        do {
            readSize = flib_read(inputID, data, size);
            flib_write(ID, data, readSize);
        } while (readSize == size);


        flib_close(inputID);
        flib_remove(inputID);
    }

};

void heapify(int32_t *data, int64_t size, int64_t root) {
    int64_t largest = root;
    int64_t left = 2 * root + 1;
    int64_t right = 2 * root + 2;

    largest = (left < size && data[left] > data[largest]) ? left : largest;
    largest = (right < size && data[right] > data[largest]) ? right : largest;

    if (largest != root) {
        swap(&data[root], &data[largest]);
        heapify(data, size, largest);
    }
}

void heapSort(int32_t *data, int64_t size) {
    for (auto i = size / 2 - 1; i >= 0; i--)
        heapify(data, size, i);

    for (auto i = size - 1; i >= 0; i--) {
        swap(&data[0], &data[i]);
        heapify(data, i, 0);
    }
}

int32_t readInput(int32_t in_file, int32_t bytes) {
    int64_t size = (bytes - 2 * FILE_OVERHEAD) / INT32_T_BYTES;
    int32_t fileID = FIRST_TEMPORARY_FILE;
    int32_t *data = new int32_t[size];

    int64_t actualSize;
    flib_open(in_file, READ);
    do {
        actualSize = flib_read(in_file, data, size);

        heapSort(data, actualSize);

        flib_open(fileID, WRITE);
        flib_write(fileID, data, actualSize);
        flib_close(fileID);
        ++fileID;

    } while (actualSize == size); // reached EOF once actual bytes < expected bytes

    //cleanup
    flib_close(in_file);
    delete[] data;

    return fileID;
}

void merge(int32_t inputID1, int32_t inputID2, int32_t outputID, int32_t size) {
    InputStream input1(inputID1, size);
    InputStream input2(inputID2, size);
    OutputStream output(outputID, size * 2);

    while (!input1.empty() || !input2.empty()) {
        int64_t a = input1.first();
        int64_t b = input2.first();

        if (a < b) {
            output.write((int32_t) a);
            input1.shift();
        } else {
            output.write((int32_t) b);
            input2.shift();
        }
    }
    output.flush();
}

void tarant_allegra(int32_t in_file, int32_t out_file, int32_t bytes) {
    int32_t lastID = readInput(in_file, bytes);

    int64_t filesCount = lastID - FIRST_TEMPORARY_FILE;
    int64_t file = FIRST_TEMPORARY_FILE;

    int32_t streamSize = (bytes - 3 * FILE_OVERHEAD) / (INT32_T_BYTES * 4);

    while (filesCount > 1) {
        int32_t filesCreated = 0; //number of files for next iteration
        int64_t outputID = file + filesCount; // id of the first output file
        int64_t firstReadFile = file;
        file = outputID;

        // one pass - merge all existing files
        for (auto i = 0; i < ceil(double(filesCount) / 2); ++i) {
            int64_t firstFile = firstReadFile + 2 * i;
            if (firstFile + 1 == file) {
                --file;
                ++filesCreated;
                break;
            }
            int32_t secondFile = firstFile + 1;
            merge(firstFile, secondFile, outputID, streamSize);
            ++outputID;
            ++filesCreated;
        }

        filesCount = filesCreated;
    }

    OutputStream outputStream(out_file, streamSize);
    outputStream.copy(file);
}

#ifndef __PROGTEST__

uint64_t total_sum_mod;

void create_random(int output, int size) {
    total_sum_mod = 0;
    flib_open(output, WRITE);
    /* srand(time(NULL)); */
#define STEP 100ll
    int val[STEP];
    for (int i = 0; i < size; i += STEP) {
        for (int j = 0; j < STEP && i + j < size; ++j) {
            val[j] = -1000 + (rand() % (2 * 1000 + 1));
            total_sum_mod += val[j];
        }
        flib_write(output, val, (STEP < size - i) ? STEP : size - i);
    }
    flib_close(output);
}

void tarant_allegra(int in_file, int out_file, int bytes);

void check_result(int out_file, int SIZE) {
    flib_open(out_file, READ);
    int q[30], loaded, last = -(1 << 30), total = 0;
    uint64_t current_sum_mod = 0;
    while (loaded = flib_read(out_file, q, 30), loaded != 0) {
        total += loaded;
        for (int i = 0; i < loaded; ++i) {
            if (last > q[i]) {
                printf("the result file contains numbers %d and %d on position %d in the wrong order!\n", last, q[i],
                       i - 1);
                exit(1);
            }
            last = q[i];
            current_sum_mod += q[i];
        }
    }
    if (total != SIZE) {
        printf("the output contains %d but the input had %d numbers\n", total, SIZE);
        exit(1);
    }
    if (current_sum_mod != total_sum_mod) {
        printf("the output numbers are not the same as the input numbers\n");
        exit(1);
    }
    flib_close(out_file);
}

int main(int argc, char **argv) {
    const uint16_t MAX_FILES = 65535;
    flib_init_files(MAX_FILES);
    int INPUT = 0;
    int RESULT = 1;
    int SIZE = 1000;

    create_random(INPUT, SIZE);
    tarant_allegra(INPUT, RESULT, 100);
    check_result(RESULT, SIZE);

    flib_free_files();

    return 0;
}

#endif //__PROGTEST__
