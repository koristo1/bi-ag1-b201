#include <iostream>
#include <map>
#include <set>
#include <queue>
#include <stack>
#include <algorithm>


using namespace std;

enum Type {
    NORMAL, INFECTED, CURE
};

struct Visit {
    bool healthyVisit;
    bool sickVisit;

    pair<int, int> infectedVisit;
    int healthyPred;
};

class Planet {
    Type type;

public:
    vector<int> neighbours;

    Planet() : type(NORMAL) {}

    void setType(Type _type);

    void addNeighbour(int id);

    Type getType();
};

void Planet::setType(Type _type) { type = _type; }

Type Planet::getType() { return type; }

void Planet::addNeighbour(int id) { neighbours.emplace_back(id); }


class Navigation {
    int vertices;
    int edges;

    int source;
    int target;
    int lives;

    vector<Planet> planets;
    vector<Visit> visited;
    queue<pair<int, int>> q;


public:
    Navigation() : vertices(0), edges(0), source(0), target(0), lives(0) {}

    void navigate();

    void getNavigation();

    void traceBack(int i, int _lives);

    bool visit(int n, int countdown, int curr);
};

void Navigation::getNavigation() {

    cin >> vertices >> edges;

    planets.resize(vertices);


    cin >> source >> target >> lives;

    int k;
    cin >> k;

    for (auto i = 0; i < k; ++i) {
        int tmp;
        cin >> tmp;
        planets[tmp].setType(INFECTED);
    }

    int d;
    cin >> d;

    for (auto i = 0; i < d; ++i) {
        int tmp;
        cin >> tmp;
        planets[tmp].setType(CURE);
    }

    for (auto i = 0; i < edges; ++i) {
        int a, b;
        cin >> a >> b;
        planets[a].addNeighbour(b);
        planets[b].addNeighbour(a);
    }
}


void Navigation::navigate() {

    q.push({source, lives});
    visited.resize(vertices);
    visited[source].healthyVisit = true;
    visited[source].healthyPred = -1;

    while (!q.empty()) {
        auto curr_id = q.front().first;
        auto curr = planets[curr_id];
        auto cntdwn = q.front().second;
        q.pop();
        for (int n : curr.neighbours)
            if (visit(n, cntdwn, curr_id)) {
                traceBack(n, cntdwn);
                return;
            }
    }
    cout << -1 << endl;
}

void Navigation::traceBack(int i, int _lives) {
    stack<int> journey;
    auto curr = i;
    auto lvs = visited[i].sickVisit ? visited[i].infectedVisit.second : lives;
    journey.push(i);
    bool healthyMode = lvs == lives;

    while (true) {
        if (visited[curr].healthyPred == -1)
            break;

        if (healthyMode) {
            auto tmp = visited[curr].healthyPred;
            if (tmp == -2) {
                tmp = visited[curr].infectedVisit.first;
                lvs = visited[curr].infectedVisit.second + 1;
                healthyMode = lvs == lives;
                curr = tmp;
                journey.push(curr);
                continue;
            } else {
                curr = visited[curr].healthyPred;
                journey.push(curr);
                continue;
            }
        } else {
            auto tmp = visited[curr].infectedVisit.first;
            lvs = visited[curr].infectedVisit.second + 1;
            healthyMode = lvs == lives;
            curr = tmp;
            journey.push(curr);
            continue;
        }

    }

    while (!journey.empty()) {
        auto j = journey.top();
        cout << j << " ";
        journey.pop();
    }
    cout << endl;
}

bool Navigation::visit(int n, int countdown, int curr) {
    auto type = planets[curr].getType();

    if (countdown == lives) {
        if (type != INFECTED) {
            if (visited[n].healthyVisit)
                return false;
            visited[n].healthyVisit = true;
            visited[n].healthyPred = curr;
            q.push({n, lives});
        }
        else {
            if ( visited[n].healthyVisit || (visited[n].sickVisit && visited[n].infectedVisit.second >= countdown))
                return false;
            if (countdown - 1 == 0 && planets[n].getType() != CURE && n != target)
                return false;
            if(countdown == 0 && planets[n].getType() != CURE && curr!= target)
                return false;
            visited[n].sickVisit = true;
            visited[n].infectedVisit = {curr, lives - 1};
            q.push({n, lives - 1});
        }
    }
    else if (type == CURE) {
        if (visited[n].healthyVisit)
            return false;
        visited[curr].healthyPred = -2;
        visited[curr].healthyVisit = true;
        visited[n].healthyVisit = true;
        visited[n].healthyPred = curr;
        q.push({n, lives});
    }
    else {
        if ((visited[n].sickVisit && visited[n].infectedVisit.second >= countdown) || visited[n].healthyVisit)
            return false;
        if (countdown - 1 == 0 && planets[n].getType() != CURE && n != target)
            return false;
        visited[n].sickVisit = true;
        visited[n].infectedVisit = {curr, countdown - 1};
        q.push({n, countdown - 1});
    }
    return n == target;
}


int main() {
    Navigation nav;
    nav.getNavigation();
    nav.navigate();
    return EXIT_SUCCESS;
}
